/* Moose Programming Language
 * (c) Jeremy Tregunna, 2006, All Rights Reserved.
 */

#ifndef __MOOSE__PLATFORM_H
#define __MOOSE__PLATFORM_H

#ifdef __cplusplus
# define MOOSE_BEGIN_DECLS extern "C" {
# define MOOSE_END_DECLS   }
#else
# define MOOSE_BEGIN_DECLS
# define MOOSE_END_DECLS
#endif /* __cplusplus */

/* Unix platforms that don't -Dunix */
#ifndef unix
# ifdef __APPLE__
/* Make sure we're running OS X previous versions of Mac OS define __APPLE__ */
#  ifdef __MACH__
#   define unix
#  else
#   error "Moose is not supported on your version of Mac OS."
#  endif /* __MACH__ */
# endif /* __APPLE__ */
# ifdef __NetBSD__
#  define unix
# endif /* __NetBSD__ */
# ifdef __unix__
#  define unix
# endif /* __unix__ */
#endif /* !unix */

/* Assertaion C99 compatibility */
#if !(defined(__STDC__) && defined(__STDC_VERSION__) \
		&& __STDC_VERSION__ >= 199901L)
# error "An ISO-C99 compiler is required."
#endif

#if defined(WIN32)
# if defined(__MINGW__) || defined(_MSC_VER)
#  define MOOSE_INLINE __inline
# else
#  define MOOSE_INLINE inline
# endif
# ifdef MOOSE_EXPORTS
#  define MOOSE_API   __declspec(dllexport)
# else
#  define MOOSE_API   __declspec(dllimport)
# endif
#else
# define MOOSE_INLINE inline
# define MOOSE_API    
#endif

#endif /* __MOOSE__PLATFORM_H */
